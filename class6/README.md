# Exercise 1 

## What is the relationship between Quantopian and Zipline?

The relationship between Quantopian and Zipline is that Quantopian is a company 
that provides everything you need to write a high-quality algorithmic trading 
strategy. You can do your research using a variety of data sources,test your 
strategy over historical data, and then test it going forward with live data. 
In this context Quantopian developed and continuously updated Zipline that is a 
open-source algorithmic trading simulator written in Python, that
powers the backtester in the IDE (interactive development environment).

# Exercise 2

## What does the set_slippage function do?

The set_slippage function does the calculation of the realistic impact of your
orders on the execution price you receive. It also evaluates if your order is
simply too big (you can't trade more than market's volume, and generally you
can't expect to trade more than a fraction of the volume).

Slippage must be defined in the initialize method. It has no effect if defined
elsewhere in your algorithm. If you do not specify a slippage method, slippage 
defaults to VolumeShareSlippage(volume_limit=0.025, price_impact=0.1) 
(you can take up to 2.5% of a minute's trade volume).

To set slippage, use the set_slippage method and pass in 
FixedBasisPointsSlippage, FixedSlippage, VolumeShareSlippage, or a 
custom slippage model that you define.

```py

def initialize(context):
    set_slippage(slippage.FixedBasisPointsSlippage(basis_points=5, volume_limit=0.1))

```

# Exercise 3 and 4

```py

from zipline.api import order, symbol
from zipline.finance import commission, slippage

stocks = ['AAPL', 'MSFT']


def initialize(context):
    context.has_ordered = False
    context.stocks = stocks
    context.set_commission(commission.PerShare(cost=.0075, min_trade_cost=1.0))
    context.set_slippage(slippage.VolumeShareSlippage())

def handle_data(context, data):
    if not context.has_ordered:
        for stock in context.stocks:
            order(symbol(stock), 100)
        context.has_ordered = True

def _test_args():
    import pandas as pd

    return {
        'start': pd.Timestamp('2008', tz='utc'),
        'end': pd.Timestamp('2013', tz='utc'),
    }

```

## What is this handle_data function doing? What is the trading strategy here?





## Is the _test_args function part of the zipline framework? is this function required to run a trading algorithm?

