# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 17:01:10 2018

@author: Luiz Henrique
"""

# Class 3 - LU4

# Index: a - 0; b - 1; c - 2

# Tuples are not suppose to be changed - they are imutable! 

apple = (101, 102, 103, 104, 105)

# Tuples are not suppose to be sorted, because they are imutable. 
# You can find ways - turn it into a list, sorted, turn it into a tuple. 
# REMEMBER: Tuples are imutable! 

apple_list = [101, 102, 103, 104, 105]
apple_list.append(106)
print(apple_list)

# Lists are dynamic and to be changed. Tuples are imutable and should not change
# You can find different consequences of this statement

t = (1, 2, 3)
l = list(t)
print(type(l), l)

# Printing keys 
d = {'a':1, 'b':2}
print(d.keys())

# Printing values
d.values()

# printing items
d.items()

# creating an empty d
empty_d = {}
empty_d['hello'] = 'world'
empty_d['why'] = 'because so'

# when you run the code you get the value of the key 'APPL'

# Lists
list_one = []
list_two = list()

# dictionaries
dict_one = {}
dict_two = dict()

# tuples
t_one = ()
t_two = tuple()

# not much creativity here... I know

# try del and pop
random_dict = {'a':1, 'b':2, 'c':3}
random_dict.pop('a')
del random_dict['b']
# we should only have c in random_dict
print(random_dict)

# Append homie
def random(list_argument):
    list_argument.append('homie')
    return list_argument
print(random([1,2,3]))

# Let's do it with a list

def sum_stocks(list_stocks):
    return sum(list_stocks)
    