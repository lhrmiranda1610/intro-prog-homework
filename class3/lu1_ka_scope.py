# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 17:00:35 2018

@author: Luiz Henrique
"""

# Class 3 - LU1

# Global: first_var; second_var
# local of hello_world: who
# local of hello_other_world: who_again

# The code runs: error

g = 10

def mult(g):
    return g*2
new = mult(g)
print(new, g)

# print(_power_) will give an error because it's only availabe in generate_power 

# power changes the value in genera_power scope. THat's the value that it is used
# inside nth_power()

def name(first_name='Elon', second_name='Musk'):
    print(first_name, second_name)
    
name()
name(first_name='Ricardo')
name(first_name='Ricardo', second_name='Pereira')

def greetings(message='Good morning', name=""):
    print(message, name)

greetings(name='John Snow', message='You know nothing')