# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 17:02:08 2018

@author: Luiz Henrique
"""
#Exercise 1
print (bool(1))
print (bool(1.1))
print (bool(0))
print (bool(0.0001))
print (bool(''))
print (bool(None))

#Exercise 2
def is_apple (ts):
    if ts=='APPL': 
        return True
    else: 
        return False

print (is_apple('APPL'))
print (is_apple('Tesla'))

#Exercise 3
def my_gender (gender):
    if gender == "Male" or gender == "M":
        print ('You hate pink')
    elif gender == "Female" or gender == "F":
        print ('You like pink')
    else: 
        print ('You don`t have a color')

my_gender("M")
my_gender("Male")
my_gender("F")
my_gender("Female")
my_gender("MF")

# Exercise 4
def my_gender (gender):
    if gender == "Male" or gender == "M":
        print ('You hate pink')
    if gender == "Female" or gender == "F":
        print ('You like pink')
    else: 
        print ("This line should not print")

my_gender("Male")

#Exercise 5
def parents_age (dad_age, mon_age):
    if dad_age == mon_age:
        return dad_age + mon_age
    elif dad_age != mon_age:
        return dad_age - mon_age

print (parents_age(30,30))
print (parents_age(50,40))

#Exercise 6
def compare_dict_with_list (list, dict):
    if dict.items() == list.len():
        print ("awesome")
    else:
        print ('that is sad')

print(compare_dict_with_list(list(1,2,3), dict(A:1,B:2,C:3)))

#Exercise 7
d_stock_price = {'APPL':100,'GOOG':150,'FB':200}

for key in d_stock_price:
    print (d_stock_price[key])

for key in d_stock_price:
    print ("the stock priceof", key, "is", d_stock_price[key],"where", key, "and",d_stock_price[key],"are the company stock symbols and stock prices respectively")
    
for key in d_stock_price:
    print (key)
    
for items in d_stock_price:
    print (items)

#Exercise 8
t = (10,20,30)
l = [40,50,60]
d = {"APPL":100,"GOOG":90,"FB":80}

for num in t:
    print (num)
    
for num in l:
    print (num)

for key in d:
    print (d[key])

#Exercise 9


#Exercise 10
    
#Exercise 11