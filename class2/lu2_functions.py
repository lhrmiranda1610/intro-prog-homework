# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 16:58:19 2018

@author: Luiz Henrique
"""

# Learning Unit #2

# exercise 1
def power(a,b):
    return a**b

# exercise 2
def verifier(param1, param2):
    return param1 == param2

# exercise 3
def pythagoras(side1, side2):
    hypotenuse2 = side1**2 + side2**2
    hypotenuse = hypotenuse2 ** (1/2)
    return hypotenuse

# exercise 4

# BTC TO EUR
# 1BTC = 7000€

def btc_to_eur(nr_of_btc):
    eur = 7000 * nr_of_btc
    return eur

# exercise 5

# Python functions always return something. If it is not defined by the user,
# returns None


# exercise 6    
# Print displays it's arguments on the screen and return is used to return
# a value from a function. 

# exercise 7
# The REPL prints [OUT] when the expression on the REPL evaluates to not null