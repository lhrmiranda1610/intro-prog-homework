# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 16:57:29 2018

@author: Luiz Henrique
"""

# Learning Unit #1

# exercise 1
ticker = "AAPL" 
stock_value = 100.0
nr_share = 50
action = "buy"
print("I would like to", action,
      nr_share, "shares in", ticker)

# exercise 2 
person1 = "Jeff Bezos"
person2 = "Elon Musk"
x = 100
y = 10
company = "Tesla" 
print(person1, "needs to borrow", x,
      "euros from", person2, 
      "in order to trade", y,
      "stocks in", company)  

# exercise 3
celsius = 30
far = celsius * (9/5) + 32
print(far)
kelvin = celsius + 274.15
print(kelvin)
