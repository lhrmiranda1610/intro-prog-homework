# Algo Strategy

It`s a every day run algo at the market open that when the current price of a stock (symbol stock) is 3% above the 10 day average price, it opens a long position. If the current price is below the average price, it closes the position to 0 shares.

## First Backtest
Stock Symbol: TSLA
Settings: From 2017-01-01 to 2018-01-01 with $1,000 initial capital
Calendar: US Equities

### Results

Total Returns - (13.25%)
Benchmark Returns - 21.7%

## Second Backtest
Stock Symbol: TSLA
Settings: From 2017-01-01 to 2018-01-01 with $1,000 initial capital
Calendar: US Equities

### Results

Total Returns - 11.80%
Benchmark Returns - 21.7%