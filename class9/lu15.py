LU15 - Quantopian - Algo Strategy

def initialize(context):
   #We select the securities that we want
    context.security = symbol('TSLA')
   
   #Schedule the fuction that will run every day at market open
    schedule_function(func=action_function,
                     date_rule=date_rules.every_day(),
                     time_rule=time_rules.market_open(hours=0, minutes=1)
                     )
def action_function(context, data):
    #Prices for the last 5 days for our security
    price_history = data.history(assets=context.security,
                                 fields='price',
                                 bar_count=10,
                                 frequency='1d'
                                 )
    average_price = price_history.mean()
    
    current_price = data.current(assets=context.security,
                                 fields='price'
                                 )
    if current_price > average_price * 1.03:
        #We allocate our entire portfolio here
        order_target_percent(context.security, 1)
    else:
        #this will set my position to zero
        order_target_percent(context.security, 0)
        
    if data.can_trade(context.security):
        #this will buy me 1000$ in TSLA shares
        order_target_percent(context.security, 1)
        log.info('Buying + {}'.format(context.security))
    else:
        #this will set my position to zero
        order_target_percent(context.security, 0)
        log.info('Setting my position to zero on {}'.format(context.security.symbol))